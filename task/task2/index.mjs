import { upperCase, lowerCase } from "./task1.mjs";
import { trimString } from "./task2.mjs";
import { word } from "./task3.mjs";
import { sentence } from "./task4.mjs";
import { contain } from "./task5.mjs";
import { length } from "./task6.mjs";

console.log(upperCase("aashutosh"))

console.log(lowerCase("AASHUTOSH"))

console.log(trimString("   ashutosh  "))

console.log(word('Bereradasdsad'))

console.log(sentence("aashutosh nitan asutosh nitan"))

console.log(contain('admins'))

console.log(length("qwewrtyuiopkjhgfdzcvbncvhbnc"))
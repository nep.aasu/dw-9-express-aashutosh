// make a arrow function that takes a input and 
// return another string which is a trim version (remove both start and end space)

export let trimString =(_trimString)=>{
    let __trimString = _trimString.trim()
    return __trimString
}

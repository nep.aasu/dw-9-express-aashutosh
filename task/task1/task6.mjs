// make a arrow function that take a number and 
// return you can enter room only if the enter number is less than 18 else you can not enter

export let number = (num) => {
    if (num >= 1 && num < 18) {
        return ("You can enter the room.")
    }
    else {
        return ("You cannot enter the room.")
    }
}


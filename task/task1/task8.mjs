// make a arrow function that takes one input as number and return "category1" for number range from 1 to10,  
// return "category2" for number range from 11 to 20, return "category3" for number range form 21 to 30

export let category =(_category)=>{
if (_category>1 && _category<=10){
    return ('category1')
}
else if (_category>=11 && _category<=20){
    return ('category2')
}
else if( _category>=21 && _category<=30){
    return ('category3')
}
else {
    return ('Please Enter Category')
}

}


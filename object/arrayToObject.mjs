// we cannot convert all array to object
// can only convert if we have array like[["name","ram"],["age",29]]
// here we have array of array and input array has length of 2

let ar = [
  ["name", "aashutosh"],
  ["age", 29],
];

console.log(Object.fromEntries(ar));

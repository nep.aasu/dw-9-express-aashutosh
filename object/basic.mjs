// array is made by the combination of value where as object is made by combination of key value pair
// name:key,aashutosh:key 
// name:"aashutosh" =>property

// let obj = {
//     name: "aashutosh",
//     age: "23",
//     isMarried: false
// }

// get whole object
// console.log(obj)

// get specific element
// console.log(obj.name)
// console.log(obj.age)
// console.log(obj.isMarried)


// change specific element
// obj.name= 'AA'
// console.log(obj)

// delete specific element
// delete obj.name
// console.log(obj)

// let obj ={
//     name:"aashutosh",
//     location:"bkt",
//     contactNumber:"9860678385"
// }

// console.log(obj)
// obj.location="gagalphedi"
// console.log(obj)

// delete obj.contactNumber
// console.log(obj)

// let obj= {
//     name:"aashutosh",
//     age:24
// }

// obj.address="bkt"
// console.log(obj)

/* let currDate = new Date();
console.log(currDate) */
/* 
2023-11-07T03:07:41.855Z
yyyy-mm-ddThh:mm:ss iso format */

// let currDate = new Date().toLocaleString();
// console.log(currDate)

// let currDate = new Date().toLocaleDateString();
// console.log(currDate)

// let currDate = new Date().toLocaleTimeString();
// console.log(currDate);

// let currDate = new Date().toLocaleTimeString();
// console.log(currDate);
let months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "Nov",
  "December",
];

let days = [
  "Sunday",
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
];
let date = new Date();
console.log(date);

let localTime = date.toLocaleString();
console.log(localTime);

let fullYear = date.getFullYear();
console.log(fullYear);

let month = months[date.getMonth()];
console.log("**");
console.log(month);

let day = days[date.getDay()];
console.log(day);

let hours = date.getHours();
console.log(hours);

let minutes = date.getMinutes();
console.log(minutes);

let seconds = date.getSeconds();
console.log(seconds);

let milliSeconds = date.getMilliseconds();
console.log(milliSeconds);

// The getTime() method returns the number of milliseconds since January 1, 1970:
let time = date.getTime();
console.log(time);

let dates = new Date(time);
console.log(dates.toString());

console.log(date.getDay());
console.log(date.getMonth());

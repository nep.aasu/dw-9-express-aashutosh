// primitive  create two memory when doing "let"

// Non primitive share memory when doing let

// primitive
// incase of primitive "===" checks the value is same or not
// let a = 1;
// let b = a;
// a = 3;
// console.log(a);
// console.log(b);

// let a = 1;
// let b = a;
// let c = 1;

// console.log(a === b);
// console.log(a === c);

// non-primitive
// in case no non primitive "===" checks the memory allocation is same or not
// let ar1 = [1, 2];
// let ar2 = ar1;
// let ar3 = [3, 4];
// ar1.push(3);
// console.log(ar1);
// console.log(ar2);
// console.log(ar3);

// let ar1 = [1, 2];
// let ar2 = ar1;
// let ar3 = [1, 2];

// console.log(ar1 === ar2);
// console.log(ar1 === ar3);

// [1,2]=[1,2]
// in this case there is no memory allocated so false

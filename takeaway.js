//js is a single threaded (one complete then another)synchronous(line ) blocking language.by line
// js is a case sensitive

// data :
// number (123)
// String(''," ")
// boolean(true,false)

// operator
// +-%*
// 1+2=3
// '1'+'2'='12'
// '1'+2='12'
// if there is war between string and number always string wins;
// operation are done between two values(operands);

// variable using let
// define variable, call variable
// difference between variable and string
//  variable is container which store data.

// operator
// && => T (all case true)
// || => T (any one true)

// rules to make variables
//  descriptive
//  camel case convention
// don't redefine variable

// conversion

// console.log(Number('1'))
// console.log(String(1))
// console.log(String('false'))
// console.log(Boolean(""))
// Boolean
// number case all empty are 0 other are true
// 0>f
// 1>t
// 2>t
// 3>t

// string case all empty are false other true
// '' f
// 'a 't
// '  't
// '0't

// the code after return doesn't execute
//function with return (call with storing variable)
// function without return(call without storing variable)

//array can store multiple value of different datatype
// array is made by the combination of value where as object is made by combination of key value pair
// all method will return something but ppus change original array while reverse and sort do both

// number sort does not work properly in js
// let ar1 = [9, 10]
// let ar2 = ar1.sort()
// console.log(ar2)

// if end index is missing slice method will slice to the end

// rest operator takes rest of values if rest of the value is in collection it keeps in array[ ] and if the value is in key  value pair it keeps in object{}

// primitive  create two memory when doing "let"
// Non primitive share memory when doing let

// interview question asynchronous Function
// settimeout and set interval always execute at last

// any thing that push task to the background(node) is asynchronous Function

// call stack : run code
// event loop : it constantly monitor call stack if the call stack is empty it takes the function from memory queue which is in background(node) and add it to the call stack
/* 


set only remove duplicate value of primitive data type except null
 */

// null

// NaN

/* CRUD operation
Create->post
Read->get
Update->patch
delete->delete
*/

/* 
Backend 
make api 
defining task for each request is called making api

postman 
hit api
test api


gitlab 


git commands 
git add .
git commit -m " message "
git push origin 


pull code 

git clone link
git pull origin 

branch

git checkout -b BRANCH NAME 
git branch no of branch
git checkout BRANCH NAME switch branch


git add . 
git commit (push,new branch, branch switch)

git init -> initialize git in a folder
*/

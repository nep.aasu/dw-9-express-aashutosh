// [1,2,1,["a","b"],["a","b"], ["a","b"]]===[1,2,["a","b"]]

let ar1 = [1, 2, 1, ["a", "b"], ["a", "b"], ["a", "b"], 1];

// let ar3 = ar1.map((value, i) => {
//   let ar4 = JSON.stringify(value);
//   console.log(ar4);
//   return ar4;
// });

// let ar2 = [...new Set(ar3)];
// console.log(ar2);

// let output = ar2.map((value, i) => {
//   return JSON.parse(value);
// });
// console.log(output);

// Alternative

let output = (input) => {
  let ar1 = input.map((value, i) => {
    return JSON.stringify(value);
  });

  let ar2 = [...new Set(ar1)];

  let ar3 = ar2.map((value, i) => {
    return JSON.parse(value);
  });

  return ar3;
};
console.log(output([1, 2, 1, ["a", "b"], ["a", "b"], ["a", "b"], 1]));

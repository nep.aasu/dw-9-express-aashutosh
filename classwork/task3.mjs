// let input = "my name is aashutosh"
// let inputArr = input.split(" ")
// console.log(inputArr)
// let inputArr1 = inputArr.map((value,i)=>{
    
//     return value.split('')
// })
// let input2 = inputArr1
// console.log(input2)

// let input3 = input2.map((value,i)=>{
//     if (i===0){
//         return value.toUpperCase()
//     }
//     else {
//     return value.toLowerCase( )}
// let outputAr = input3.join("")
// })
// console.log(input3)

let firstLetterCapital = (input)=>{
    let inputAr = input.split("");

    let outputAr = inputAr.map((value,i)=>{
        if (i === 0){
            return value.toUpperCase()
        }
        else {
            return value.toLowerCase()
        }
    })

    let output = outputAr.join("")

    return output;
    
};

let eachWordCapital = (input)=>{
    let inputAr = input.split(" ");

    let inputAr1 = inputAr.map((value,i)=>{
        return firstLetterCapital(value);
    })

    let output = inputAr1.join(" ");
    return output;
}

console.log(eachWordCapital("hello my name is john doe"))
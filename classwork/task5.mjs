// make a function that takes any  number of inputs and returns thr sum of all those inputs

let sum = (...value) => {
  let _sum = value.reduce((pre, cur) => {
    return pre + cur;
  }, 0);
  return _sum;
};
console.log(`the sum is : ${sum(1, 2, 3, 4, 5, 6, 7)}`);

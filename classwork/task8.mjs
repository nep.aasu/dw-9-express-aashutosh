let info = [
  {
    name: "nitan",
    gender: "male",
  },
  {
    name: "hari",
    gender: "male",
  },
  {
    name: "gita",
    gender: "female",
  },
  {
    name: "sita",
    gender: "female",
  },
  {
    name: "utshab",
    gender: "other",
  },
];
// using reduce method

// let value = info.reduce((pre, cur) => {
//   return pre * cur;
// }, 1);
// console.log(value);

const groupByGender = info.reduce((result, current) => {
  // Check if the gender key already exists in the result
  if (!result[current.gender]) {
    // If not, create a new key with an array containing the current element
    result[current.gender] = [current];
  } else {
    // If the key already exists, push the current element to the existing array
    result[current.gender].push(current);
  }
  return result;
}, {});

console.log(groupByGender);

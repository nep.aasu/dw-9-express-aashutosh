// interview

console.log("a"); // first run this
setTimeout(() => {
  // settimeout function is pass to background(node) and attach timer to it
  console.log("hello");
}, 1000);

console.log("b"); // then this line will run
//  after the last line settime out func is return
/*
a
b
hello 
*/
// settimeout and set interval always execute at last
